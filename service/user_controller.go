package service

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"task2/model"
	"task2/provider"
	"text/template"
	"time"
)

func HandleView()http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {
		list_users := provider.ViewUsers()

		buf, er := ioutil.ReadFile("task2/index.html")
		if er != nil {
			log.Fatal("Error load HTML")
		}

		var page = model.IndexPage{AllUserss: list_users}
		indexPage := string(buf)
		t := template.Must(template.New("indexPage").Parse(indexPage))
		t.Execute(w, page)
	}
}

func HandleInsert()http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {

		no_tlp, err := strconv.Atoi(r.FormValue("no_tlp"))
		if err != nil {
			log.Fatal("Error Conv. int no_tlp")
		}
		no_ktp, err := strconv.Atoi(r.FormValue("no_ktp"))
		if err != nil {
			log.Fatal("Error Conv. int no_ktp")
		}

		created, er := time.Parse("2006-01-02", r.FormValue("created_at"))
		if er != nil {
			log.Fatal("Error Conv. date create")
		}
		updated, er := time.Parse("2006-01-02", r.FormValue("updated_at"))
		if er != nil {
			log.Fatal("Error Conv. date update")
		}

		var users model.Users
		users.Firstname = r.FormValue("firstname")
		users.Lastname = r.FormValue("lastname")
		users.Email = r.FormValue("email")
		users.No_tlp = no_tlp
		users.No_tlp = no_ktp
		users.Created_at = created
		users.Updated_at = updated

		provider.CreateUser(users.Firstname, users.Lastname, users.Email, users.No_tlp, users.No_tlp, users.Created_at, users.Updated_at)

		http.Redirect(w, r, "/view", 302)
	}
}

func HandleUpdate()http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {

		id, err := strconv.Atoi(r.FormValue("user_id"))
		no_tlp, err := strconv.Atoi(r.FormValue("no_tlp"))
		no_ktp, err := strconv.Atoi(r.FormValue("no_ktp"))
		if err != nil {
			log.Fatal("Error Conv. int")
		}

		created, er := time.Parse("2006-01-02", r.FormValue("created_at"))
		updated, er := time.Parse("2006-01-02", r.FormValue("updated_at"))
		if er != nil {
			log.Fatal("Error Conv. date")
		}

		var users model.Users
		users.User_id = id
		users.Firstname = r.FormValue("firstname")
		users.Lastname = r.FormValue("lastname")
		users.Email = r.FormValue("email")
		users.No_tlp = no_tlp
		users.No_ktp = no_ktp
		users.Created_at = created
		users.Updated_at = updated

		row, err := provider.UpdateUser(users.Firstname, users.Lastname, users.Email, users.No_tlp, users.No_ktp, users.Created_at, users.Updated_at, users.User_id)
		if err != nil {
			log.Fatal("Error")
		}

		fmt.Printf("Rows update: %v\n", row)

		http.Redirect(w, r, "/view", 302)
	}
}

func HandleDelete()http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {
		id := r.FormValue("user_id")

		if len(id) > 0 {
			id, err := strconv.Atoi(id)
			if err != nil {
				log.Fatal("Error")
			}

			row, err := provider.DeleteUser(id)
			if err != nil {
				log.Fatal("Error")
			}

			fmt.Printf("Rows removed: %v\n", row)
		}
		http.Redirect(w, r, "/view", 302)

	}
}