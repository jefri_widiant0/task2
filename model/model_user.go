package model

import (
	"time"
)

type Users struct {
	User_id int `json:"user_id"`
	Firstname string `json:"firstname"`
	Lastname string `json:"lastname"`
	Email string `json:"email"`
	No_tlp int `json:"no_tlp"`
	No_ktp int `json:"no_ktp"`
	Created_at time.Time `json:"created_at"`
	Updated_at time.Time `json:"updated_at"`
}

type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    []Users
}

type IndexPage struct {
	AllUserss []Users
}