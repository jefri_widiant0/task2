package provider

import (
	"database/sql"
	"log"
	"task2/config"
	"task2/model"
	"time"
)

func ViewUsers() ([]model.Users) {

	var users model.Users
	var arr_user []model.Users
	var response model.Response

	connect := config.Connect()
	defer connect.Close()

	rows, err := connect.Query("Select id_user, firstname, lastname, email, no_tlp, no_ktp, created_at, updated_at from t_user")
	println(err)
	if err != nil {
		log.Print(err)
	}

	for rows.Next() {
		if err := rows.Scan(&users.User_id, &users.Firstname, &users.Lastname, &users.Email, &users.No_ktp, &users.No_tlp, &users.Created_at, &users.Updated_at); err != nil {
			log.Fatal(err.Error())

		} else {
			arr_user = append(arr_user, users)
		}
	}

	response.Status = 1
	response.Message = "Success"
	response.Data = arr_user

	return arr_user

	//w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(response)
}

func CreateUser(firstname, lastname, email string, no_ktp, no_tlp int, created_at, updated_at time.Time)(*sql.DB, error){
		connect := config.Connect()
		defer connect.Close()

		_, err := connect.Exec("INSERT INTO  t_user(firstname, lastname, email, no_tlp, no_ktp, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)", firstname, lastname, email, no_ktp, no_tlp, created_at, updated_at)
		if err != nil{
			log.Fatal("Error Connection!")
		}

	return connect, nil
}

func UpdateUser(firstname, lastname, email string, no_tlp, no_ktp int, created_at, updated_at time.Time, id int )(int, error){
		connect := config.Connect()
		defer connect.Close()

		_, err := connect.Exec("UPDATE t_user SET firstname = $1, lastname = $2, email = $3, no_tlp = $4, no_ktp = $5, created_at = $6, updated_at = $7 WHERE id_user = $8", firstname, lastname, email, no_tlp, no_ktp, created_at, updated_at, id)
		if err != nil{
			log.Fatal("Error!")
		}

	return int(id), nil
}


func DeleteUser(id int) (int, error) {
	connect := config.Connect()
	defer connect.Close()

	_, err := connect.Exec("DELETE FROM t_user WHERE id_user = $1", id)
	if err != nil {
		log.Fatal("Error1 Conv.!")
	}

	return int(id), nil
}
