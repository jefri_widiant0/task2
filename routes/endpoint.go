package main

import (
	"net/http"
	"task2/service"
)

func rooting()  {

	http.HandleFunc("/view", service.HandleView())
	http.HandleFunc("/create", service.HandleInsert())
	http.HandleFunc("/update", service.HandleUpdate())
	http.HandleFunc("/delete", service.HandleDelete())

	http.ListenAndServe(":3000", nil)
}

func main() {
	rooting()
}
